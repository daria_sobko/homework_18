package User;

public class User2 extends User {
    private double salary;
    private double experience;

    public User2(String firstName, String lastName, int workPhone, int mobilePhone, String email, String password,
                 double salary, double experience) {
        super(firstName, lastName, workPhone, mobilePhone, email, password);
        this.salary = salary;
        this.experience = experience;
    }

    public double getSalary() {
        return salary;
    }

    public double getExperience() {
        return experience;
    }

    public double setSalary() {
        if (experience < 2 && experience !=0) {
            return salary = salary + (salary * 0.05);
        } else if (experience >= 2 && experience <=5 ) {
            return salary = salary + (salary * 0.1);
        } else if (experience > 5) {
            return salary = salary + (salary * 0.15);
        } else {
            return 0;
        }
    }

    /*Check*/
    public static void main(String[] args) {

        User2 Test2 = new User2("Test", "Test", 1, 2, "test@gmail.com",
                "test", 10000, 3);
        System.out.println (Test2.setSalary());
    }
}